import IOrder from "../../model/order";
const ordersDomain = require('../../domain/orders/orders');

// 1 -> Orders, 3 -> controller, 1 -> n_file
const PREFIX_COMMON_ERROR = exports.PREFIX_COMMON_ERROR = "131";

// ERRORS
const ERROR_ORDER_NOT_FOUND = exports.ERROR_ORDER_NOT_FOUND = "001";
const ERROR_ORDERS_NOT_FOUND = exports.ERROR_ORDER_NOT_FOUND = "002";
const ERROR_COULD_NOT_CREATE_ORDER = exports.ERROR_COULD_NOT_CREATE_ORDER = "003";
const ERROR_COULD_NOT_UPDATE_ORDER = exports.ERROR_COULD_NOT_UPDATE_ORDER = "004";
const ERROR_COULD_NOT_DELETE_ORDER = exports.ERROR_COULD_NOT_DELETE_ORDER = "005";

exports.getOrders = async () => {
    const data: IOrder[] = await ordersDomain.getOrders();
    if (!data || data.length <= 0) throw {
        internalCode: PREFIX_COMMON_ERROR + ERROR_ORDERS_NOT_FOUND, code: 404,
        message: "ERROR_ORDERS_NOT_FOUND"
    };
    return data;
}

exports.getOrderById = async (id: string) => {
    const data: IOrder = await ordersDomain.getOrderById(id);
    if (!data) throw {
        internalCode: PREFIX_COMMON_ERROR + ERROR_ORDER_NOT_FOUND, code: 404,
        message: "ERROR_ORDER_NOT_FOUND"
    };
    return data;
}

exports.createOrder = async (order: IOrder) => {
    const data: IOrder[] = await ordersDomain.createOrder(order);
    if (!data) throw {
        internalCode: PREFIX_COMMON_ERROR + ERROR_COULD_NOT_CREATE_ORDER, code: 500,
        message: "ERROR_COULD_NOT_CREATE_ORDER"
    };
    return data;
}

exports.updateOrder = async (order: IOrder) => {
    const data: IOrder[] = await ordersDomain.updateOrder(order);
    if (!data) throw {
        internalCode: PREFIX_COMMON_ERROR + ERROR_COULD_NOT_UPDATE_ORDER, code: 500,
        message: "ERROR_COULD_NOT_UPDATE_ORDER"
    };
    return data;
}

exports.deleteOrder = async (id: string) => {
    const data: IOrder[] = await ordersDomain.deleteOrder(id);
    if (!data) throw {
        internalCode: PREFIX_COMMON_ERROR + ERROR_COULD_NOT_DELETE_ORDER, code: 500,
        message: "ERROR_COULD_NOT_DELETE_ORDER"
    };
    return data;
}
