const winston = require('winston');
const morgan = require('morgan');
const path = require('path');
const constants = require('../../configuration/constants');
const COMMON_ID = 'bob-challenge-be';

var winstonLogger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'debug',
            filename: path.join("./", constants.LOGS_DIR_NAME, '/', constants.LOGS_FILE_NAME),
            handleExceptions: true,
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 5,
            colorize: false,
            timestamp: true
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true,
            timestamp: true
        })
    ],
    exitOnError: false
});

winstonLogger.stream = {
    write: (message: string, encoding: string) => winstonLogger.info(message)
};

// Wrap Winston logger to print reqId in each log
const formatMessage = function (message: string, level: string, id: string) {
    const completeLog = {
        timestamp: new Date().toUTCString(),
        level,
        logger: `${COMMON_ID}/${id}`,
        message,
        userId: ' - not logged -'
    }
    return completeLog;
};

const logger = {
    log: (level: string, message: string, id: string) => winstonLogger.log(level, formatMessage(message, level, id)),
    error: (message: string, id: string) => winstonLogger.error(formatMessage(message, 'ERROR', id)),
    warn: (message: string, id: string) => winstonLogger.warn(formatMessage(message, 'WARN', id)),
    verbose: (message: string, id: string) => winstonLogger.verbose(formatMessage(message, 'VERBOSE', id)),
    info: (message: string, id: string) => winstonLogger.info(formatMessage(message, 'INFO', id)),
    debug: (message: string, id: string) => winstonLogger.debug(formatMessage(message, 'DEBUG', id)),
    silly: (message: string, id: string) => winstonLogger.silly(formatMessage(message, 'SILLY', id)),
    webservices: () => morgan('dev', { stream: winstonLogger.stream })
};

export default logger;