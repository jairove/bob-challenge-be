require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

const toolsDB = require('../db_tools');
import IOrder from '../../../model/order';

const db = toolsDB.getDBConnection();
let ordersCollection: any;
try {
    ordersCollection = db.collection('orders');
    db.createCollection("orders");
} catch (e) { /*TODO Handle error*/ }

exports.getOrders = async (): Promise<IOrder> =>
    await ordersCollection.find().toArray();

exports.getOrderById = async (id: string): Promise<IOrder> =>
    await ordersCollection.findOne({ _id: ObjectId(id) });

exports.createOrder = async (order: IOrder) => {
    const result = await ordersCollection.insertOne(order);
    return result.insertedId;
};

exports.updateOrder = async (order: IOrder) =>
    await ordersCollection.findOneAndUpdate({ _id: ObjectId(order._id) }, { $set: order });

exports.deleteOrder = async (id: string) =>
    await ordersCollection.delete({ _id: ObjectId(id) });

export { };