import logger from '../../logs/logger';
import IOrder from '../../../model/order';
const dbTools = require('../db_tools');

const LOG_ID = 'db_init'

const MOCKED_ORDERS: IOrder[] = [
    { name: 'John Doe', bags: 4 },
    { name: 'Jane Smith', bags: 1 },
    { name: 'Amanda Hawkes', bags: 2 },
    { name: 'Stuart Johnson', bags: 6 }
]

const initializeOrders = async () => {
    try {
        await dbTools.createDBConnection();
        const ordersDomain = require('../../../domain/orders/orders');
        MOCKED_ORDERS.forEach((order: IOrder) => ordersDomain.createOrder(order))
    } catch (error) {
        logger.error(error, LOG_ID)
    }
}

initializeOrders()