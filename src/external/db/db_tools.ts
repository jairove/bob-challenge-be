/*
 * Created by jvelasco on 2020-05-27
 */
const MongoClient = require('mongodb').MongoClient;

const config = require('../../configuration/config.json');
import logger from '../logs/logger';

const MONGODB_URL = 'mongodb://';
const LOG_ID = 'external/bd/db_tools'
let db: any;

exports.getDBConnection = () => db;

exports.createDBConnection = async () => {

    const getDBUrl = () => {
        let dbURL = MONGODB_URL;
        if (process.env.DB_USER && process.env.DB_PASSWORD &&
            process.env.DB_USER != '' && process.env.DB_PASSWORD !== '') {
            dbURL += `${process.env.DB_USER}:${process.env.DB_PASSWORD}@`;
        }

        const port = config.db_config.port ? `:${config.db_config.port}` : '';
        dbURL += config.db_config.host + port + '/' + config.db_config.db;

        if (config.db_config.params) {
            dbURL += `?${config.db_config.params}`;
        }
        return dbURL;
    }

    if (db) return new Promise((resolve, reject) => resolve(db));
    const dbURL = getDBUrl();
    const options = {
        useUnifiedTopology: true
    }
    logger.info(`Connecting to DB on: ${config.db_config.host}:${config.db_config.port}`, LOG_ID)
    const dbConnection = await MongoClient.connect(dbURL, options);
    if (!dbConnection) {
        logger.info('Error connecting DB', LOG_ID);
        throw Error('Error connecting DB');
    }
    logger.info("DB connected successfully", LOG_ID);
    db = dbConnection.db(config.db_config.db);

    return db;
};

export { }