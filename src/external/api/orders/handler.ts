import { Request, Response, NextFunction } from 'express';
const ordersController = require('../../../controller/orders/orders');

exports.getOrders = async (req: Request, res: Response, next: NextFunction) => {
    const result = await ordersController.getOrders();
    res.status(200).json(result)
}

exports.getOrder = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;
    const result = await ordersController.getOrderById(id);
    res.status(200).json(result)
}

exports.createOrder = async (req: Request, res: Response, next: NextFunction) => {

}

exports.updateOrder = async (req: Request, res: Response, next: NextFunction) => {
    //TODO
}

exports.deleteOrder = async (req: Request, res: Response, next: NextFunction) => {
    //TODO
}