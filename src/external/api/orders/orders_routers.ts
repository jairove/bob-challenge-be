import express from 'express';
const asyncHandler = require('express-async-handler')

const handler = require('./handler');
const constants = require('../../../configuration/constants');

const ORDERS_ROUTE = `${constants.API_PATH}${constants.API_VERSION}/orders`;

exports.loadRoutes = ((app: express.Application) => {
    app.get(`${ORDERS_ROUTE}`, asyncHandler(handler.getOrders));
    app.get(`${ORDERS_ROUTE}/:id`, asyncHandler(handler.getOrderById));
    app.post(`${ORDERS_ROUTE}`, asyncHandler(handler.createOrder));
    app.put(`${ORDERS_ROUTE}`, asyncHandler(handler.updateOrder));
    app.delete(`${ORDERS_ROUTE}/:id`, asyncHandler(handler.deleteOrder));
})