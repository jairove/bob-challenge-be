import express from 'express';
const config = require('../../configuration/config');

interface IModule {
    key: string,
    active: boolean,
    paths: string[]
}

exports.assignRoutes = (app: express.Application) =>
    config.MODULES
        .filter((moduleConfig: IModule) => moduleConfig.active)
        .reduce((paths: string[], modulePath: IModule) => [...paths, ...modulePath.paths], [])
        .map((path: string) => require(path).loadRoutes(app))