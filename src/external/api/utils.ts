import { Request, Response } from 'express';
const statuses = require('statuses');
import logger from '../logs/logger'
const LOG_ID = 'external/api/utils';
const DEFAULT_ERROR_CODE = 500;

/**
 * Generic method to treat errors and send a responde with a concrete format.
 *
 * @error Object Error raised from code
 * @req Object Request object
 * @res Object Response object
 * @logError Boolean If the error has to be logged or not
 * @defaultErrorCode Number In case error does not have a code, this param will be used. Default code is 500
 */
exports.manageError = function (error: any, req: Request, res: Response, logError = true,
    defaultErrorCode = DEFAULT_ERROR_CODE) {
    let errorCode = error.code ? error.code : defaultErrorCode;
    let message = error.message ? error.message.toString() : error.toString();

    // If it is a mongo validation error, return 400 as http code
    if (error.name && error.name === 'MongoError' && error.code && error.code === 121) {
        errorCode = 400;
        message = "Bad request";
    }

    let statusCode;

    // Check statusCode is a valid HTTP status code.
    // If not, force status code to DEFAULT_ERROR_CODE.
    try {
        statuses(errorCode);
        statusCode = errorCode;
    } catch (ex) {
        statusCode = DEFAULT_ERROR_CODE;
    }

    if (logError) {
        const logMessage = error.internalCode ? message + '. Internal code: ' + error.internalCode : message;
        logger.error(logMessage, LOG_ID);
    }
    return res.status(statusCode).send({ message: message, code: errorCode });
}