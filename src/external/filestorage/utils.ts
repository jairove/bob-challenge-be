const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');

import logger from '../logs/logger';
const config = require('../../configuration/config')

const LOG_ID = 'external/filestorage/utils';

exports.createFolder = (foldername: string) =>
    mkdirp(foldername, (err: string) =>
        err ? logger.error(err, LOG_ID) : logger.info("The folder: " + foldername + " was created", LOG_ID)
    );

exports.removeFile = (filepath: string) => fs.unlinkSync(filepath)

exports.storeFile = async (file: any) => {
    const destDir = config.file_storage.dir;
    const destPath = path.join(destDir, file.filename + '.' + file.originalname.split('.')[1]);

    await storeFileLocally(file.path, destPath);

    return destPath;
}

const storeFileLocally = async (filePath: string, destPath: string) => {
    if (!fs.existsSync(destPath)) fs.mkdirSync(destPath);
    await fs.renameSync(filePath, destPath);
}