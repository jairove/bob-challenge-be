export default interface IOrder {
    _id?: string;
    name: string;
    bags: number;
}