import express, { Request, Response, NextFunction } from 'express';
const cors = require('cors');
const bodyparser = require('body-parser');
const routes = require('./external/api/routes');
const apiUtils = require('./external/api/utils');
import logger from './external/logs/logger';
const config = require('./configuration/config.json');
const dbTools = require('./external/db/db_tools')
const fileUtils = require('./external/filestorage/utils');

const app: express.Application = express();
const LOG_ID = 'server';

const start = async () => {
    if (!config.db_config.mock) await dbTools.createDBConnection();
    fileUtils.createFolder("./logs");

    // configure app to use bodyParser()
    // this will let us get the data from a POST
    app.use(bodyparser.urlencoded({ extended: true }));
    app.use(bodyparser.json({ limit: '10mb' }));
    app.use(logger.webservices());

    // aplying CORS
    const corsOptions = {
        origin: true,
        credentials: true
    };
    app.use(cors(corsOptions));

    // Loading all routes
    routes.assignRoutes(app);
    app.listen(config.internal_access.port);

    // Common Error Handling
    app.use((err: any, req: Request, res: Response, next: NextFunction) =>
        apiUtils.manageError(err, req, res, true, 500))

    logger.info(`Server listening on port ${config.internal_access.port}`, LOG_ID);
}

start();