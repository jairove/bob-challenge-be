const assert = require('chai').assert;
const expect = require('chai').expect;
import sinon from 'sinon';
import IOrder from '../../model/order';

export const DUMMY_ORDERS: IOrder[] = [{ _id: '5', name: 'John Smith', bags: 5 }, { _id: '1', name: 'John Watson', bags: 1 }]

describe('DOMAIN - Orders', () => {

    let ordersDomain: any;
    let ordersDb: any;
    let stubs: sinon.SinonStub[] = [];

    before(() => {
        ordersDomain = require('../../domain/orders/orders');
        ordersDb = require('../../external/db/orders/orders');
    });

    afterEach(() => stubs.forEach(stub => stub.restore()));

    describe('Get', () => {
        it('Order by id - OK', async () => {
            stubs.push(sinon.stub(ordersDb, 'getOrderById').returns(new Promise((res) => res(DUMMY_ORDERS[0]))));
            let data = await ordersDomain.getOrderById('5');
            assert(data, DUMMY_ORDERS[0])
        });


        it('Orders - OK', async () => {
            stubs.push(sinon.stub(ordersDb, 'getOrders').returns(new Promise((res) => res(DUMMY_ORDERS))));
            let data = await ordersDomain.getOrders();
            assert(data, DUMMY_ORDERS)
        });

        it('Orders - No data', async () => {
            stubs.push(sinon.stub(ordersDb, 'getOrders').returns(new Promise((res) => res([]))));
            let data = await ordersDomain.getOrders();
            assert(data, []);
        });
    })
});