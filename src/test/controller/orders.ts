const assert = require('chai').assert;
const expect = require('chai').expect;
import sinon from 'sinon';
import { DUMMY_ORDERS } from '../domain/orders';

describe('CONTROLLER - Orders', () => {

    let ordersController: any;
    let ordersDomain: any;
    let stubs: sinon.SinonStub[] = [];

    before(() => {
        ordersController = require('../../controller/orders/orders');
        ordersDomain = require('../../domain/orders/orders');
    });

    afterEach(() => stubs.forEach(stub => stub.restore()));

    describe('Get', () => {
        it('Orders - OK', async () => {
            stubs.push(sinon.stub(ordersDomain, 'getOrders').returns(DUMMY_ORDERS));
            try {
                let data = await ordersController.getOrders();
                assert(stubs[0].called);
                assert(data, DUMMY_ORDERS)
            } catch (error) {
                assert(false)
            }
        });

        it('Orders - No data', async () => {
            stubs.push(sinon.stub(ordersDomain, 'getOrders').returns([]));
            try {
                await ordersController.getOrders();
                assert(false)
            } catch (error) {
                assert(error.code, 404)
            }
        });

        it('Order by id - OK', async () => {
            stubs.push(sinon.stub(ordersDomain, 'getOrders').returns(DUMMY_ORDERS));
            try {
                let data = await ordersController.getOrders();
                assert(stubs[0].called);
                assert(data, DUMMY_ORDERS)
            } catch (error) {
                assert(false)
            }
        });

        it('Order by id - No data', async () => {
            stubs.push(sinon.stub(ordersDomain, 'getOrders').returns([]));
            try {
                await ordersController.getOrders('924');
                assert(false)
            } catch (error) {
                assert(error.code, 404)
            }
        });
    })

    describe('Post', () => {
        it('Order - OK', async () => {
            stubs.push(sinon.stub(ordersDomain, 'createOrder').returns(DUMMY_ORDERS[1]));
            try {
                let data = await ordersController.createOrder(DUMMY_ORDERS[1]);
                assert(stubs[0].called);
                assert(data, DUMMY_ORDERS[1])
            } catch (error) {
                assert(false)
            }
        });
        it('Order - Not OK', async () => {
            stubs.push(sinon.stub(ordersDomain, 'createOrder').returns([]));
            try {
                await ordersController.createOrder({});
                assert(stubs[0].called);
                assert(false)
            } catch (error) {
                assert(true)
            }
        });
    })
});