const ordersDb = require('../../external/db/orders/orders');
import IOrder from '../../model/order';

// 1 -> Orders, 2 -> domain, 1 -> n_file
const PREFIX_COMMON_ERROR = exports.PREFIX_COMMON_ERROR = "121";

const ERROR_DB_UNAVAILABLE = exports.ERROR_DB_UNAVAILABLE = "001";

exports.getOrders = async (): Promise<IOrder[] | null> => {
    let data = null;
    try {
        data = await ordersDb.getOrders();
    } catch (error) {
        // TODO - Complete error with for further details
        throw {
            internalCode: PREFIX_COMMON_ERROR + ERROR_DB_UNAVAILABLE, code: 503,
            message: "SERVICE_UNAVAILABLE", i18n: true
        };
    }
    return data;
}

exports.getOrderById = async (id: string): Promise<IOrder | null> =>
    await ordersDb.getOrderById(id)

exports.deleteOrder = async (id: string): Promise<IOrder | null> =>
    await ordersDb.deleteOrder(id)

exports.createOrder = async (order: IOrder): Promise<IOrder | null> =>
    await ordersDb.createOrder(order)

exports.updateOrder = async (order: IOrder): Promise<IOrder | null> =>
    await ordersDb.updateOrder(order)
