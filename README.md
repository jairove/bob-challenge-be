# BoB Challenge Backend

## Usage

### Launch

1. **Download repo**
2. **Install dependencies**
   ```npm install```
3. **Configuration**
    - Make sure you have an available mongo instance.
    - Set up proper configuration variables in /configurarion/config.json
    - For the next step you'll need to have ts-node installed:
        npm install -g ts-node
    - Initialize database by running db_init:
        ts-node src/external/db/init/db_init.ts
4. **Run dev**
   ```npm run dev```

### Testing

You need node & npm installed in your computer

- To run unit tests
    ```npm test```

## Development

### Tech stack
 - NodeJS (v12.16.1+)
 - Typescript (v3+)
 - MongoDB

*Look at package.json for further details on libraries*


### Architecture

This project have been developed following clean architecture

- External folder contains those pieces of the code that interact with external libraries/dependencies
- Controller layer/folder maps the functionality bound to each API resource
- Domain layer/folder contains small delimited parts of business rules
- Model layer/folder if needed would contain definition of the core entities of our project

### API Docs

To be added

